# README #

Data Loader

### What is this repository for? ###

##### Quick summary #####

Generator that load data to database Mysql or SQLlite from csv file.
![alt text](images/schema.png)

### How do I get set up? ###
###### Configuration ######
Configure the file LoadDat.conf

###### Dependencies ######

Ruby
###### Database configuration ######

Install Mysql and configure the config file with the connection

Install SQLite and configure the config file with the connection

###### How to run tests ######

Run the script file from terminal: ./script.sh

Specify the configuration file "LoadData.conf" that contains the data to save in the database installed in the system 
