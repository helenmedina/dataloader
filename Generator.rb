require 'erb'

class Generator
	
	def initialize(dbConf, fileConf, connectorConf, columnsConf, doubleColumns)

		@dbConf = dbConf
		@fileConf = fileConf
		@connectorConf = connectorConf
		@columnsConf = columnsConf
		@doubleColumns = doubleColumns
		@isInteger = false
		@isString = false
		@isDate = false
		@isDouble = false
		#Look what data types are defined in the data file in order to build the source files for the specific data type.
		self.findTypes
		
	end
	
	def findTypes
	
		@columnsConf.each do |column|
			if (column[:type] == 'int')
				@isInteger = true
			elsif (column[:type] == 'QString')
				@isString = true
			elsif (column[:type] == 'QDateTime')
				@isDate = true
			elsif (column[:type] == 'double')
				@isDouble = true
			end
		end

	end
	
	def generateProjectConfiguration
		
		class_name = "LoadFile.pro"
		fh = File.open( "./templates/#{class_name}" )
		loadFile = fh.read()
		fh.close
		
		#Build the path for the driver and headers
		loadFile.gsub!( /(#\s*)(INCLUDEPATH|LIB)(.*?)\n/m){
		if($2 == "INCLUDEPATH")
			code = "#{$2} += -I#{@connectorConf[:include_path]}\n"
		elsif($2 == "LIB")
			code = "#{$2} += -I#{@connectorConf[:lib_path]} -#{@connectorConf[:driver]}\n"
		end
		
		}
		
		print "Creating #{class_name}\n"
		File.open( "./output/#{class_name}", "w" ).write( loadFile )
		
	end
	
	def generateProject
	
		self.generateProjectConfiguration
		self.generateFile("loader.h")
		self.generateFile("main.cpp")
		self.generateFile("loader.cpp")
		
	end
	
	def generateFile(fileName)
	
		#create the ERB object to bind the arrays 
		erb = ERB.new( File.open( "./templates/#{fileName}" ).read, nil, '-' )
		loadFile = erb.result( binding )
		
		print "Creating #{fileName}\n"

		#create the output file with the arrays and template information
		File.open( "./output/#{fileName}", "w" ).write( loadFile )
		
	end

end
