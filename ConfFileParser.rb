class FileNotExist < StandardError

	attr_accessor :data
	
	def initialize(filename)
		@data = filename
	end
  
end



class ConfFileParser

	Sdatabase = Struct.new(:connector, :hostname, :dbServer, :userName, :password, :tableName) 
	Sfile = Struct.new(:pathFile, :logFile, :separator) 
	SConnector = Struct.new(:driver, :include_path, :lib_path)

	attr_accessor :file

	def initialize(file)
		@file = file
		@databaseOptions = ['connector', 'hostname', 'database_server', 'user_name', 'password', 'table_name']
		@fileOptions = ['pathBase', 'fileName', 'logFile', 'separator']
		@connectorSettings = ['driver', 'conn_include_path', 'conn_lib_path']
		@databaseConfig = []
		@fileConfig = []
		@connectorConfig = []
		
	end
	
	def getDatabaseConfig
		return @databaseConfig
	end
	
	def getFileConfig
		return @fileConfig
	end
	
	def getConnectorConfig
		return @connectorConfig
	end
	
	def parse
		index = 0
		if (File.exist?(@file))
			lines = File.readlines(@file)
			#load database settings from config file
			connector = findMatch(lines, @databaseOptions[index])
			hostname = findMatch(lines, @databaseOptions[index + 1])
			dbServer = findMatch(lines, @databaseOptions[index + 2])
			userName = findMatch(lines, @databaseOptions[index + 3])
			password = findMatch(lines, @databaseOptions[index + 4])
			tableName = findMatch(lines, @databaseOptions[index + 5])
			
			@databaseConfig = Sdatabase.new(connector, hostname, dbServer, userName, password, tableName) 

			#load connector settings
			driver = findMatch(lines, @connectorSettings[index])
			includePath = findMatch(lines, @connectorSettings[index + 1])
			libPath = findMatch(lines, @connectorSettings[index + 2])

			@connectorConfig = SConnector.new(driver, includePath, libPath) 
			
			#load file settings from config file
			pathBase = findMatch(lines, @fileOptions[index])
			fileName = getPathFile(findMatch(lines, @fileOptions[index + 1]))
			@AbspathFile = getAbsPath(pathBase, fileName)
			
			#If log file path is not found, a default path is defined
			logFile = getLogfile(findMatch(lines, @fileOptions[index + 2]))
			
			#Read separator from config file
			separator = getSeparator(findMatch(lines, @fileOptions[index + 3]))

			@fileConfig = Sfile.new(@AbspathFile, logFile, separator) 
		else
			raise FileNotExist.new(@file)
		end


	end
	
	def findMatch(lines, expression)
	
		#Find the line that matches with the expresion and then get the value after the operator '='
		match = nil
		lines.each do |line|
			if (line =~ /^#{expression}\s+=\s+(.*?)$/i) || (line =~ /^#{expression}\s+=(.*?)$/i)
				match = $1
			end
		end
		
		return match
		
	end
	
	def getSeparator(expression)
	
		#Get the separator defined that split the fields in the data file
		if (expression =~ /^'(.*?)'$/i)
			expression = $1
		end
		
		return expression
		
	end
	
	def getLogfile(log)
		
		if (log == nil || log.empty?)
			log = "./log.txt"
		end
		
		return log
		
	end
	
	def getPathFile(expression)
	
		#Get file name relative
		nameFile = expression.split("/").last

		return nameFile
		
	end
	
	def getAbsPath(pathBase, file)
		path = "#{pathBase}/input/#{file}"
		
		return path
	end

end
