#ifndef LOGGER
#define LOGGER
#include<string>
#include <iostream>
#include <fstream>

class Logger
{

public:
    static Logger* GetLogger(std::string);
    Logger& operator<<(std::string);
    Logger& operator<<(int);
    void Close();
    
private:
    Logger(std::string);
    Logger(Logger const&);
    Logger& operator=(Logger const&);
    
private:
    std::ofstream myfile;

public:
    static Logger* logger;

};

#endif // LOGGER

