#include <QCoreApplication>
#include <QtSql>
#include "loader.h"


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    
    //Setting connection parameters
    QSqlDatabase db = QSqlDatabase::addDatabase("<%= @dbConf.connector %>");
    db.setHostName("<%= @dbConf.hostname %>");
    db.setDatabaseName("<%= @dbConf.dbServer %>");<% if @dbConf.userName != nil && !@dbConf.userName.empty?%><%="\n    db.setUserName(\"#{@dbConf.userName}\");"%><% end %><% if @dbConf.password != nil && !@dbConf.password.empty?%><%="\n    db.setPassword(\"#{@dbConf.password}\");"%><% end %>
    Loader loader("<%= @fileConf.pathFile %>", "<%= @fileConf.separator %>"<%- if @fileConf.logFile != nil && !@fileConf.logFile.empty?-%>, "<%= @fileConf.logFile %>"<%- end -%>);

    if (!db.open()) {
        qDebug() << db.lastError();
    }
    else
    {
        qDebug() << "Connected";
        loader.SetTable("<%= @dbConf.tableName %>");
        loader.LoadData(db);
        db.close();
    }

    a.exit();
}

