#ifndef LOADER_H
#define LOADER_H
#include <limits>
#include "logger.h"

class QString;
class QSqlDatabase;
class QSqlQuery;
class QStringList;
class QFile;
class QDateTime;

<%- @columnsConf.each do |column| -%>
<%- if (column.type == 'int' || column.type == 'QString') -%>
#define MAX_SIZE_COLUMN_<%=  column.columnUpperCase %> <%= column.format %> 
<%- end -%>
<%- end -%>

typedef struct fields_t {
	<% @columnsConf.each do |column| %>
	<%= column.type %> st_<%= column.columnName %>;<% end %>
	
}stFields;


class Loader
{

public:
    Loader(QString, QString, std::string logFile  = "<%= @fileConf.logFile %>");
    //Load the data file and save the fields for a specific database
    void LoadData(QSqlDatabase&);
    
    //Build the query to insert the fields for a specific table
    QString GetInsertQuery();
    
    //Getters and setters
    QString GetTable();
    void SetTable(QString);
<%- @doubleColumns.each do |column| -%>
    int GetTop_<%= column.columnName %>();
    int GetBottom_<%= column.columnName %>();
    int GetPrecision_<%= column.columnName %>();
    void SetTop_<%= column.columnName %>(int);
    void SetBottom_<%= column.columnName %>(int);
    void SetPrecision_<%= column.columnName %>(int);
<%- end -%>

private:
    void BindValues(stFields, QSqlQuery query);
    //Execute the insert query and commit the changes
    void ExecuteQuery(QSqlQuery, QString, QFile& file);
    
    //Get the fields from a list split by a separator and validate them
    fields_t BuildTableFields(QStringList);
    
    //validate table fields
<%- if (@isInteger) -%>
    void CheckField(int, int, std::string);
<%- end -%>
<%- if (@isString) -%>
    void CheckField(QString, int, std::string);
<%- end -%>
<%- if (@isDate) -%>
    void CheckField(QDateTime, std::string);
<%- end -%>
<%- if (@isDouble) -%>
    void CheckField(double, std::string, int iBottom = std::numeric_limits<int>::min() , int iTop = std::numeric_limits<int>::max(), int iPrecision = 10);
<%- end -%>


private:
    QString strPathFile;
    QString strTableName;
    QString strSeparator;
    std::string strLogFile;
    Logger* logger;
    bool bPassValidation;
<%- @doubleColumns.each do |column| -%>
    int iBottom_<%= column.columnName %>;
    int iTop_<%= column.columnName %>;
    int iPrecision_<%= column.columnName %>;
<%- end -%>

};

#endif // LOADER_H

