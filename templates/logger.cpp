#include <logger.h>

Logger* Logger::logger = nullptr;

Logger::Logger(std::string  path)
{
    myfile.open (path);

}

Logger* Logger::GetLogger(std::string path)
{
    if(logger == nullptr)
    {
        logger = new Logger(path);
    }
    return logger;
}


Logger& Logger::operator<<(std::string str)
{
    if(myfile.is_open()) {
             myfile << str;
     }

    return *logger;
}

Logger& Logger::operator<<(int number)
{
    if(myfile.is_open()) {
             myfile << number;
     }

    return *logger;
}

void Logger::Close()
{
        myfile.close();
        delete logger;
}
