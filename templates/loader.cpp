#include <QString>
#include <QtSql>
#include <QException>
#include <QDoubleValidator>
#include "loader.h"

Loader::Loader(QString pathFile, QString separator, std::string logFile)
    : strPathFile(pathFile), strSeparator(separator),  strLogFile(logFile)
{
    bPassValidation = true;
    //Set the double fields with the lowest value, highest value and the precision allowed
<%- @doubleColumns.each do |column| -%>
    SetBottom_<%= column.columnName %>(<%= column.bottom %>);
    SetTop_<%= column.columnName %>(<%= column.top %>);
    SetPrecision_<%= column.columnName %>(<%= column.precision %>);
<%- end -%>
}

void Loader::LoadData(QSqlDatabase& db)
{
    QFile file(strPathFile);
    QSqlQuery query(db);

    //If log path is not specified, it is generated one by default
    logger= Logger::GetLogger(strLogFile);
    
    //Open the data file, otherwise log the error
    if(!file.open(QIODevice::ReadOnly)) {
        *logger <<  file.errorString().toStdString() << ": " << strPathFile.toStdString();
    }
    else
    {
        try
        {
            QString strInsertQuery = GetInsertQuery();
            ExecuteQuery(query, strInsertQuery, file);
        }
        catch(QException &e)
        {
            qDebug() << e.what();
            file.close();
        }
        catch(...)
        {
            qDebug() << "LoadData() - Unknown exception. For more details see log file: " << QString::fromStdString(strLogFile);
            file.close();
        }
         file.close();
    }

    logger->Close();
    db.close();

}

void Loader::ExecuteQuery(QSqlQuery query, QString strInsertQuery, QFile& file)
{
    QTextStream in(&file);
    QString line = in.readLine();
    query.prepare(strInsertQuery);

     while (!in.atEnd())
     {
         line = in.readLine();
         bPassValidation = true;
         QStringList fields = line.split(strSeparator);
         stFields columns = BuildTableFields(fields);
         
         if (!bPassValidation)
            continue;
         else
         {
            qDebug() << "The application has errors. For more details see log file: " << QString::fromStdString(strLogFile);
         }
            
         BindValues(columns, query);
         
         if (query.exec())
         {
             *logger << "success\n";
         }
         else
         {
             *logger << query.lastError().text().toStdString() << "\n";
             qDebug() << query.lastError().text();
             throw QSqlError();
         }
     }
}

QString Loader::GetInsertQuery()
{
    QString query = <%="\"INSERT INTO #{ @dbConf.tableName } ("%> <% @columnsConf.each_with_index do |column, i| %><%= column.columnName %><%=", " if i < (@columnsConf.size - 1)%><%end %> <%=")\""%>
                    <%="\"VALUES ("%> <% @columnsConf.each_with_index do |column, i| %><%=":"%><%= column.columnName %><%=", " if i < (@columnsConf.size - 1)%><%end %> <%=")\";"%>

    qDebug() << query;
    
    return query;
}

void Loader::BindValues(stFields fields, QSqlQuery query)
{   <% @columnsConf.each do |column| %>
    <%=- "query.bindValue(\":#{column.columnName}\", fields.st_#{column.columnName});" -%>
    <% end %>
    
    *logger <<"\n";
}

fields_t Loader::BuildTableFields(QStringList fields)
{
    int initialPos = 0;
    stFields columns;

<%- @columnsConf.each do |column| -%>
<%- if (column.type == 'int') -%>
    columns.st_<%= column.columnName %> = (fields.at(initialPos++)).toInt();
    CheckField(columns.st_<%= column.columnName %>, MAX_SIZE_COLUMN_<%=  column.columnUpperCase %>, "st_<%= column.columnName %>");
    
<%- elsif (column.type == 'QString') -%>
    columns.st_<%= column.columnName %> = (fields.at(initialPos++));
    CheckField(columns.st_<%= column.columnName %>, MAX_SIZE_COLUMN_<%=  column.columnUpperCase %>, "st_<%= column.columnName %>");
    
<%- elsif (column.type == 'QDateTime') -%>
    columns.st_<%= column.columnName %> = QDateTime::fromString(fields.at(initialPos++),"<%= column.format %>");
    CheckField(columns.st_<%= column.columnName %>, "st_<%= column.columnName %>");
    
<%- elsif (column.type == 'double') -%>
    columns.st_<%= column.columnName %> = (fields.at(initialPos++)).toDouble();
    CheckField(columns.st_<%= column.columnName %>,"st_<%= column.columnName %>", GetBottom_<%= column.columnName %>(), GetTop_<%= column.columnName %>(), GetPrecision_<%= column.columnName %>());
    
<%- end -%>
<%- end -%>
    return columns;
}

<%- if (@isInteger) -%>
void Loader::CheckField(int iField, int maxSize, std::string description)
{
    //Get the number of digits of iField
    int length =  ( iField==0 ) ? 1 : (int)log10(iField)+1;
    if (length > maxSize)
    {
        bPassValidation = false;
         *logger << "The column \"" << description << "\": " << iField << " is not valid. The max value: " << maxSize << "\n";
    }
}

<%- end -%>
<%- if (@isString) -%>
void Loader::CheckField(QString iField, int maxSize, std::string description)
{
    int length =  iField.length();
    if (length > maxSize)
    {
        bPassValidation = false;
       *logger << "The column \"" << description << "\": " << iField.toStdString() << " is not valid. The max value: " << maxSize << "\n";
    }
}
    
<%- end -%>
<%- if (@isDouble) -%>
void Loader::CheckField(double iField, std::string strDescription, int iBottom, int iTop, int iPrecision)
{

    QString strField =  QString::number (iField);
    int pos =0;
    //The double is valid in the range [iBottom - Top] and iPrecision is the maximum allowed
    QDoubleValidator *validator = new QDoubleValidator (iBottom, iTop, iPrecision);

    if ((validator->validate(strField, pos) == (QValidator::Invalid ||  QValidator::Intermediate))  || validator->validate(strField, pos) !=QValidator::Acceptable)
    {
        bPassValidation = false;
       *logger << "The column \"" << strDescription << "\": " << strField.toStdString() << " is not valid. \n";
    }
}

<%- end -%>
<%- if (@isDate) -%>
void Loader::CheckField(QDateTime  iField, std::string description)
{

    if (!iField.isValid())
    {
        bPassValidation = false;
       *logger << "The date format for \""<< description <<  "\" is not valid \n";
    }

}

<%- end -%>
void Loader::SetTable(QString strTableName)
{
    this->strTableName = strTableName;
}

QString Loader::GetTable()
{
    return this->strTableName;
}

<%- @doubleColumns.each do |column| -%>
void Loader::SetTop_<%= column.columnName %>(int iTop)
{
    iTop_<%= column.columnName %> = iTop;
}

void Loader::SetBottom_<%= column.columnName %>(int iBottom)
{
    iBottom_<%= column.columnName %> = iBottom;
}

void Loader::SetPrecision_<%= column.columnName %>(int iPrecision)
{
    iPrecision_<%= column.columnName %> = iPrecision;
}

int Loader::GetTop_<%= column.columnName %>()
{
    return iTop_<%= column.columnName %>;
}

int Loader::GetBottom_<%= column.columnName %>()
{
    return iBottom_<%= column.columnName %>;
}

int Loader::GetPrecision_<%= column.columnName %>()
{
    return iPrecision_<%= column.columnName %>;
}

<%- end -%>
