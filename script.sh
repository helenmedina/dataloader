#!/bin/bash

#Generate the output files
ruby Trabajo2.rb

if [ $? -ne 0 ]; then
	echo "Last operation failed"

else

	#copy the logger.cpp and logger.h to the output folder
	cp ./templates/logger* ./output

	#Build the project
	cd ./output
	qmake
	make

	#Run the executable
	./Connector

fi
