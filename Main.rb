require_relative 'ConfFileParser.rb'
require_relative 'DataFileParser.rb'
require_relative 'Generator.rb'

puts 'Config file to parse: '
STDOUT.flush  
fileName = gets.chomp

if not fileName.match(/.conf\z/)
 fileName = fileName + '.conf'
end

begin

	#parse the config file
	puts "loading config file \n"
	configParser = ConfFileParser.new("#{fileName}")
	configParser.parse
	
	#Contains the parameters of the section [database_server]
	dbConf = configParser.getDatabaseConfig 
	#Contains the parameters of the section [data_file]
	fileConf= configParser.getFileConfig 
	#Contains the parameters of the section [connector]
	connectorConf = configParser.getConnectorConfig
	
	#parse the data file
	puts "loading data file \n"
	separator = fileConf[:separator]
	pathFile = fileConf[:pathFile]
	
	dataFileParser = DataFileParser.new("#{pathFile}", separator)
	dataFileParser.parse
	#Contains all the columns (name, type, format) of the table
	columnsConf = dataFileParser.getColumns
	#Contains only double columns if there is any with the format (lowestValue; highestValue; precision)
	doubleColumns = dataFileParser.getDoubleColumns

	#Call the generator to generate the source and header files
	generator = Generator.new(dbConf, fileConf, connectorConf, columnsConf, doubleColumns)
	generator.generateProject
	
	
rescue FileNotExist => e
	puts "File Not exist: #{e.data}"
	return false
	
rescue Exception => e
	puts "Failed: #{e.message}"
	puts e.backtrace.inspect
	return false
end


