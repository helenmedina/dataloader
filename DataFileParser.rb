class DataFileParser

	StColumn = Struct.new(:columnName, :columnUpperCase, :type, :format)
	StDoubleColumns = Struct.new(:columnName, :bottom, :top, :precision)
	
	def initialize(file, separator)
		@file = file
		@separator = separator
		@arrayColumns = []
		@arrayDoubleColumns = []
	end
	
	def getColumns
		return @arrayColumns
	end
	
	def getDoubleColumns
		return @arrayDoubleColumns
	end
	
	def parse
	
		#Get the fields split by the separator defined in the config file
		firstLine = File.open(@file).first
		#Remove whitespaces
		firstLine = firstLine.gsub(/\s+/, "")
		firstLine.split("#{@separator}").each do|field|
			buildColumns(field)
		end
		
	end
	
	def buildColumns(field)
		
		#The data file  must have the following format "Column:Type(format)"
		temp = field.split("::")
		type = nil
		format = nil
		
		#Get the column name 
		column = temp.first
		if (temp.last =~ /^(.*?)\((.*?)\)$/)
			#Get the column type
			type = getType($1, column)
			if (type == 'double')
			
				#If the type is double, get the (lowest value; highest value; precision)
				format = $2.split(";", 3)
				bottom = format[0]
				top = format[1]
				precision = format[2]
				doubleColumns = StDoubleColumns.new(column, bottom, top, precision)
				@arrayDoubleColumns.push(doubleColumns)
				
			else
			
				#Get the format enclosed by "()"
				format = $2
				
			end
		end
		
		columns = StColumn.new(column, column.upcase, type, format) 
		@arrayColumns.push(columns)
		
	end
	
	def getType(type, column)
			
			#Get the correspondent data type for the QT creator IDE
			if(type == nil || type.empty?)
				raise Exception , "Type column \"#{column}\" is null."
			else
				case type.downcase
				when "int"
					type = 'int'
				when "double"
					type = 'double'
				when "varchar"
					type = 'QString'
				when "date"
					type = 'QDateTime'
				else
					raise Exception , "Type \"#{type}\" unknown."
				end
			end
			
		return type
		
	end
	

end
